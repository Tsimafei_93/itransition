﻿using WebApp.Data.Models;
using WebApp.Data.Repositories;

namespace WebApp.Data.UOW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        public UnitOfWork(ApplicationDbContext context) => _context = context;

        private GenericRepository<User> _users;
        public GenericRepository<User> Users
        {
            get => _users is null ? _users = new GenericRepository<User>(_context) : _users;
        }

        private GenericRepository<UserCollection> _userColl;
        public GenericRepository<UserCollection> UserCollections
        {
            get => _userColl is null ? _userColl = new GenericRepository<UserCollection>(_context) : _userColl;
        }

        private GenericRepository<Item> _item;
        public GenericRepository<Item> Items
        {
            get => _item is null ? _item = new GenericRepository<Item>(_context) : _item;
        }

        private GenericRepository<Comment> _comment;
        public GenericRepository<Comment> Comments
        {
            get => _comment is null ? _comment = new GenericRepository<Comment>(_context) : _comment;
        }

        private GenericRepository<ItemLike> _like;
        public GenericRepository<ItemLike> ItemLikes
        {
            get => _like is null ? _like = new GenericRepository<ItemLike>(_context) : _like;
        }

        private GenericRepository<ItemTag> _tag;
        public GenericRepository<ItemTag> ItemTags
        {
            get => _tag is null ? _tag = new GenericRepository<ItemTag>(_context) : _tag;
        }

        private GenericRepository<PropertyInt> _int;
        public GenericRepository<PropertyInt> PropertyInts
        {
            get => _int is null ? _int = new GenericRepository<PropertyInt>(_context) : _int;
        }

        private GenericRepository<PropertyString> _string;
        public GenericRepository<PropertyString> PropertyStrings
        {
            get => _string is null ? _string = new GenericRepository<PropertyString>(_context) : _string;
        }

        private GenericRepository<PropertyBool> _bool;
        public GenericRepository<PropertyBool> PropertyBools
        {
            get => _bool is null ? _bool = new GenericRepository<PropertyBool>(_context) : _bool;
        }

        private GenericRepository<PropertyText> _text;
        public GenericRepository<PropertyText> PropertyTexts
        {
            get => _text is null ? _text = new GenericRepository<PropertyText>(_context) : _text;
        }

        private GenericRepository<Theme> _theme;
        public GenericRepository<Theme> Themes
        {
            get => _theme is null ? _theme = new GenericRepository<Theme>(_context) : _theme;
        }

        public async Task<bool> SaveAsync()
        {
            int count = await _context.SaveChangesAsync();
            return count > 0;
        }
    }
}
