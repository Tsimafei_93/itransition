﻿using WebApp.Data.Models;
using WebApp.Data.Repositories;

namespace WebApp.Data.UOW
{
    public interface IUnitOfWork
    {
        GenericRepository<User> Users { get; }
        GenericRepository<Theme> Themes { get; }
        GenericRepository<UserCollection> UserCollections { get; }
        GenericRepository<Item> Items { get; }
        GenericRepository<Comment> Comments { get; }
        GenericRepository<ItemLike> ItemLikes { get; }
        GenericRepository<ItemTag> ItemTags { get; }
        GenericRepository<PropertyInt> PropertyInts { get; }
        GenericRepository<PropertyBool> PropertyBools { get; }
        GenericRepository<PropertyString> PropertyStrings { get; }
        GenericRepository<PropertyText> PropertyTexts { get; }
        Task<bool> SaveAsync();
    }
}
