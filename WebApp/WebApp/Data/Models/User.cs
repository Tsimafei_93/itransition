﻿using Microsoft.AspNetCore.Identity;
namespace WebApp.Data.Models
{
    public class User : IdentityUser
    {
        public DateTime? CreatedDate { get; set; }
        public virtual ICollection<UserCollection>? Collections { get; set; }
        public virtual ICollection<Comment>? Comments { get; set; }
        public virtual ICollection<ItemLike>? Likes { get; set; }
    }
}
