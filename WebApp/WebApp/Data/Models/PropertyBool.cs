﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Data.Models
{
    public class PropertyBool
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? Value { get; set; }
        virtual public UserCollection? UserCollection { get; set; }
        virtual public Item? Item { get; set; }
    }
}
