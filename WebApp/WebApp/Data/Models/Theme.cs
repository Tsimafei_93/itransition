﻿
namespace WebApp.Data.Models
{
    public class Theme
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<UserCollection>? Collections { get; set; }
    }
}
