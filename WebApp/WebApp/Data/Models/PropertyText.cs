﻿namespace WebApp.Data.Models
{
    public class PropertyText
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? Value { get; set; }
        virtual public UserCollection? UserCollection { get; set; }
        virtual public Item? Item { get; set; }
    }
}
