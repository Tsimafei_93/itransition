﻿namespace WebApp.Data.Models
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public virtual User? User { get; set; }
        public virtual Item? Items { get; set; }
    }
}
