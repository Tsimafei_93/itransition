﻿namespace WebApp.Data.Models
{
    public class ItemTag
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public virtual ICollection<Item>? Item { get; set; }
    }
}
