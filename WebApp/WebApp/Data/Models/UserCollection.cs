﻿namespace WebApp.Data.Models
{
    public class UserCollection
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UserId { get; set; }
        public string Image { get; set; }
        public virtual Theme Theme { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Item>? Items { get; set; }
        public virtual ICollection<PropertyInt>? PropInts { get; set; }
        public virtual ICollection<PropertyString>? PropStrs { get; set; }
        public virtual ICollection<PropertyBool>? PropBools { get; set; }
        public virtual ICollection<PropertyText>? PropTexts { get; set; }

    }
}
