﻿namespace WebApp.Data.Models
{
    public class PropertyInt
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? Value { get; set; }
        virtual public UserCollection? UserCollection { get; set; }
        virtual public Item? Item { get; set; }

    }
}
