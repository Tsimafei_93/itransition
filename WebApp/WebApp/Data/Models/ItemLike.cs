﻿namespace WebApp.Data.Models
{
    public class ItemLike
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public virtual User User { get; set; }
        public virtual Item Item { get; set; }

    }
}
