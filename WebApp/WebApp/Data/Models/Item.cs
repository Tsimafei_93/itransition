﻿namespace WebApp.Data.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateData { get; set; }
        public virtual UserCollection UserCollection { get; set; }
        public virtual ICollection<ItemTag>? Tags { get; set; }
        public virtual ICollection<ItemLike>? Likes { get; set; }
        public virtual ICollection<Comment>? Comments { get; set; }
        public virtual ICollection<PropertyInt>? PropInts { get; set; }
        public virtual ICollection<PropertyString>? PropStrs { get; set; }
        public virtual ICollection<PropertyBool>? PropBools { get; set; }
        public virtual ICollection<PropertyText>? PropTexts { get; set; }

    }
}
