﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApp.Data.Models;

namespace WebApp.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<PropertyInt>()
                .HasOne(p => p.Item)
                .WithMany(t => t.PropInts)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<PropertyString>()
                .HasOne(p => p.Item)
                .WithMany(t => t.PropStrs)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<PropertyBool>()
                .HasOne(p => p.Item)
                .WithMany(t => t.PropBools)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<PropertyText>()
                .HasOne(p => p.Item)
                .WithMany(t => t.PropTexts)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<ItemLike>()
                .HasOne(p => p.Item)
                .WithMany(t => t.Likes)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<UserCollection>()
               .HasOne(p => p.User)
               .WithMany(t => t.Collections)
               .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Item>()
               .HasOne(p => p.UserCollection)
               .WithMany(t => t.Items)
               .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Comment>()
               .HasOne(p => p.Items)
               .WithMany(t => t.Comments)
               .OnDelete(DeleteBehavior.Cascade);
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<ItemTag> ItemTags { get; set; }
        public DbSet<ItemLike> ItemLikes { get; set; }
        public DbSet<UserCollection> UserCollections { get; set; }
        public DbSet<Theme> Themes { get; set; }
        public DbSet<PropertyInt> IntProperties { get; set; }
        public DbSet<PropertyString> StringProperties { get; set; }
        public DbSet<PropertyBool> BoolProperties { get; set; }
        public DbSet<PropertyText> TextProperties { get; set; }

    }
}