﻿using Microsoft.EntityFrameworkCore;

namespace WebApp.Data.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T>
        where T : class
    {
        protected readonly ApplicationDbContext _context;
        public GenericRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public IQueryable<T> Get()
        {
            return _context.Set<T>();
        }

        public async Task<T> FindById(string id)
        {
            return await _context.FindAsync<T>(id);
        }

        public async Task<T> FindById(int id)
        {
            return await _context.FindAsync<T>(id);
        }

        public void Create(T item)
        {
            _context.Add(item);
        }
        public void Update(T item)
        {
            _context.Update(item);
        }
        public void Remove(T entity)
        {
            _context.Remove(entity);
        }
    }
}

