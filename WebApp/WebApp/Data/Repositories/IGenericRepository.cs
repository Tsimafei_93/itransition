﻿
namespace WebApp.Data.Repositories
{
    public interface IGenericRepository<T>
        where T : class
    {
        void Create(T entity);
        Task<T> FindById(string id);
        Task<T> FindById(int id);
        IQueryable<T> Get();
        void Remove(T entity);
        void Update(T entity);
    }
}
