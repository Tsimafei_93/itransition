﻿using Microsoft.EntityFrameworkCore;
using WebApp.Data.Models;
using WebApp.Data.UOW;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class PropIntService : IPropService<PropertyInt>
    {
        private readonly IUnitOfWork _uow;
        public PropIntService(IUnitOfWork uow)
        {
            _uow = uow;
        }


        public Task<PropertyInt> Create(PropertyInt entity)
        {
            throw new NotImplementedException();
        }

        public Task<PropertyInt> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public List<PropertyInt> Get()
        {
            throw new NotImplementedException();
        }

        public async Task<List<PropertyInt>> GetByItem(int id)
        {
            return await _uow.PropertyInts.Get()
                .Where(u => u.Item.Id == id)
                .ToListAsync();
        }

        public async Task<List<PropertyInt>> Get(int id)
        {
            return await _uow.PropertyInts.Get()
                .Where(u => u.UserCollection.Id == id)
                .Include(u => u.UserCollection)
                .ToListAsync();
        }

        public void Remove(PropertyInt entity)
        {
            throw new NotImplementedException();
        }

        public Task<PropertyInt> Update(PropertyInt entity)
        {
            throw new NotImplementedException();
        }
    }
}
