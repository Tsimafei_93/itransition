﻿using Microsoft.EntityFrameworkCore;
using WebApp.Data.Models;
using WebApp.Data.UOW;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _uow;
        public CommentService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<Comment> Create(Comment entity)
        {
            _uow.Comments.Create(entity);
            await _uow.SaveAsync();
            return entity;
        }

        public async Task<Comment> FindById(int id)
        {
            return await _uow.Comments.FindById(id);
        }

        public async Task<List<Comment>> Get(int id)
        {
            return await _uow.Comments.Get().Where(u => u.Items.Id == id).ToListAsync();
        }

        public List<Comment> Get()
        {
            return _uow.Comments.Get().ToList();
        }

        public void Remove(Comment entity)
        {
            _uow.Comments.Remove(entity);
        }

        public async Task<Comment> Update(Comment entity)
        {
            _uow.Comments.Update(entity);
            await _uow.SaveAsync();
            return entity;
        }

        public async Task<List<Comment>> Search(string text)
        {
            var list = await _uow.Comments
                 .Get().Where(f => EF.Functions.FreeText(f.Text, text))
                 .Include(u=>u.Items)
                 .ToListAsync();
            return list;
        }


    }
}
