﻿using Microsoft.EntityFrameworkCore;
using WebApp.Data.Models;
using WebApp.Data.UOW;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class LikeService : ILikeService
    {
        private readonly IUnitOfWork _uow;
        public LikeService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<ItemLike> Create(ItemLike entity)
        {
            _uow.ItemLikes.Create(entity);
            await _uow.SaveAsync();
            return entity;
        }

        public async Task<ItemLike> FindById(int id)
        {
            return await _uow.ItemLikes.FindById(id);
        }

        public async Task<List<ItemLike>> Get(int id)
        {
            return await _uow.ItemLikes.Get()
                .Where(u => u.Item.Id == id)
                .Include(u=>u.User)
                .ToListAsync();
        }

        public  List<ItemLike> Get()
        {
            return _uow.ItemLikes.Get().ToList();
        }

        public void Remove(ItemLike entity)
        {
            _uow.ItemLikes.Remove(entity);
        }

        public async Task RemoveAsync(ItemLike like)
        {
            _uow.ItemLikes.Remove(like);
            await _uow.SaveAsync();
        }

        public async Task<ItemLike> Update(ItemLike entity)
        {
            _uow.ItemLikes.Update(entity);
            await _uow.SaveAsync();
            return entity;
        }
    }
}
