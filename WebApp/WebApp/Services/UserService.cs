﻿using Microsoft.EntityFrameworkCore;
using WebApp.Data.Models;
using WebApp.Data.UOW;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;
        public UserService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<User> Create(User user)
        {
            _uow.Users.Create(user);
            return user;
        }

        public async Task<User> FindById(string id)
        {
            return await _uow.Users.FindById(id);
        }

        public async Task<User> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public List<User> Get()
        {
            var users = _uow.Users.Get().ToList();
            return users;
        }

        public void Remove(User user)
        {
            _uow.Users.Remove(user);
            _uow.SaveAsync();
        }

        public async Task RemoveAsync(User user)
        {
            _uow.Users.Remove(user);
            await _uow.SaveAsync();
        }

        public async Task<User> Update(User user)
        {
            _uow.Users.Update(user);
            await _uow.SaveAsync();
            return user;
        }

    }

}
