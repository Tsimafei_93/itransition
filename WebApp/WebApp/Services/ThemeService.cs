﻿using WebApp.Data.Models;
using WebApp.Data.UOW;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class ThemeService : IThemeService
    {
        private readonly IUnitOfWork _uow;

        public ThemeService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<Theme> Create(Theme entity)
        {
            _uow.Themes.Create(entity);
            await _uow.SaveAsync();
            return entity;
        }

        public async Task<Theme> FindById(int id)
        {
            return await _uow.Themes.FindById(id);
        }

        public List<Theme> Get()
        {
           return _uow.Themes.Get().ToList();
        }

        public void Remove(Theme entity)
        {
            throw new NotImplementedException();
        }
        public async Task RemoveAsync(Theme theme)
        {
            _uow.Themes.Remove(theme);
            await _uow.SaveAsync();
        }

        public  async Task<Theme> Update(Theme theme)
        {
            _uow.Themes.Update(theme);
            await _uow.SaveAsync();
            return theme;
        }
    }
}
