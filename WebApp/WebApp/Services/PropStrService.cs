﻿using Microsoft.EntityFrameworkCore;
using WebApp.Data.Models;
using WebApp.Data.UOW;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class PropStrService : IPropService<PropertyString>
    {
        private readonly IUnitOfWork _uow;
        public PropStrService(IUnitOfWork uow)
        {
            _uow = uow;
        }


        public Task<PropertyString> Create(PropertyString entity)
        {
            throw new NotImplementedException();
        }

        public Task<PropertyString> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public List<PropertyString> Get()
        {
            throw new NotImplementedException();
        }

        public async Task<List<PropertyString>> Get(int id)
        {
            return await _uow.PropertyStrings.Get()
                .Where(u => u.UserCollection.Id == id)
                .Include(u => u.UserCollection)
                .ToListAsync();
        }

        public async Task<List<PropertyString>> GetByItem(int id)
        {
            return await _uow.PropertyStrings.Get()
               .Where(u => u.Item.Id == id)
               .ToListAsync();
        }

        public void Remove(PropertyString entity)
        {
            throw new NotImplementedException();
        }

        public Task<PropertyString> Update(PropertyString entity)
        {
            throw new NotImplementedException();
        }
    }
}
