﻿using Microsoft.EntityFrameworkCore;
using WebApp.Data.Models;
using WebApp.Data.UOW;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class PropTextService : IPropService<PropertyText>
    {
        private readonly IUnitOfWork _uow;
        public PropTextService(IUnitOfWork uow)
        {
            _uow = uow;
        }


        public Task<PropertyText> Create(PropertyText entity)
        {
            throw new NotImplementedException();
        }

        public Task<PropertyText> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public List<PropertyText> Get()
        {
            throw new NotImplementedException();
        }

        public async Task<List<PropertyText>> Get(int id)
        {
            return await _uow.PropertyTexts.Get()
                .Where(u => u.UserCollection.Id == id)
                .Include(u => u.UserCollection)
                .ToListAsync();
        }

        public async Task<List<PropertyText>> GetByItem(int id)
        {
            return await _uow.PropertyTexts.Get()
               .Where(u => u.Item.Id == id)
               .ToListAsync();
        }

        public void Remove(PropertyText entity)
        {
            throw new NotImplementedException();
        }

        public Task<PropertyText> Update(PropertyText entity)
        {
            throw new NotImplementedException();
        }
    }
}
