﻿using Microsoft.EntityFrameworkCore;
using WebApp.Data;
using WebApp.Data.Models;
using WebApp.Data.UOW;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class UserCollectionsService : IUserCollectionService
    {
        private readonly IUnitOfWork _uow;

        public UserCollectionsService(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public async Task<UserCollection> Create(UserCollection userColl)
        {
            _uow.UserCollections.Create(userColl);
            await _uow.SaveAsync();
            return userColl;
            
        }

        public async Task<UserCollection> FindById(string id)
        {
            return await _uow.UserCollections.FindById(id);
        }

        public async Task<UserCollection> FindById(int id)
        {
            return await _uow.UserCollections.FindById(id);
        }

        public List<UserCollection> Get()
        {
            var coll = _uow.UserCollections.Get()
                .Include(u => u.User)
                .Include(u => u.Theme)
                .Include(u => u.Items)
                .ToList();
            return coll;
        }

        public async Task<List<UserCollection>> Get(string Id)
        {
            var coll = await _uow.UserCollections.Get()
                .Include(u => u.User)
                .Include(u => u.Theme)
                .Where(u=>u.User.Id==Id)
                .ToListAsync();
            return coll;
        }

        public void Remove(UserCollection coll)
        {
            _uow.UserCollections.Remove(coll);
            _uow.SaveAsync();
        }

        public async Task RemoveAsync(UserCollection coll)
        {
            var collect = _uow.UserCollections.Get()
                .Include(u => u.PropBools)
                .Include(u => u.PropInts)
                .Include(u => u.PropTexts)
                .Include(u => u.PropStrs)
                .ToList();
            foreach (var post in collect)
            {
                post.PropTexts = null;
                post.PropStrs = null;
                post.PropInts = null;
                post.PropBools = null;
            }
            await _uow.SaveAsync();

            _uow.UserCollections.Remove(coll);
            await _uow.SaveAsync();
        }

        public async Task<UserCollection> Update(UserCollection coll)
        {
            _uow.UserCollections.Update(coll);
            await _uow.SaveAsync();
            return coll;
        }

        public async Task<List<UserCollection>> Search(string text)
        {
           var list =  await _uow.UserCollections
                .Get().Where(f => EF.Functions.FreeText(f.Name, text) || EF.Functions.FreeText(f.Description, text))
                .Include(u => u.User)
                .Include(u => u.Theme).ToListAsync();
           return list;
        }
    }
}
