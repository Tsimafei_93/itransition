﻿using Microsoft.EntityFrameworkCore;
using WebApp.Data.Models;
using WebApp.Data.UOW;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class ItemService
        : IItemService
    {
        private readonly IUnitOfWork _uow;
        public ItemService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<Item> Create(Item entity)
        {
            _uow.Items.Create(entity);
            await _uow.SaveAsync();
            return entity;
        }

        public async Task<Item> FindById(string name)
        {
            return await _uow.Items.FindById(name);
        }

        public async Task<Item> FindById(int id)
        {
            return await _uow.Items.Get()
                .Include(x => x.Likes)
                .Include(x => x.Comments)
                .ThenInclude(u => u.User)
                .Include(x => x.Tags)
                .Include(x => x.PropInts)
                .Include(x => x.PropBools)
                .Include(x => x.PropStrs)
                .Include(x => x.PropTexts)
                .FirstOrDefaultAsync(u => u.Id == id);
        }

        public List<Item> Get()
        {
            var items = _uow.Items.Get()
                .Include(x => x.Likes)
                .Include(x => x.Comments)
                .ThenInclude(u => u.User)
                .Include(x => x.Tags)
                .ToList();
            return items;
            ;
        }

        public async Task<List<Item>> GetColl(int id)
        {
            var items = await _uow.Items.Get()
                .Where(u => u.UserCollection.Id == id)
                .Include(x => x.Likes)
                .Include(x => x.UserCollection)
                .ThenInclude(u => u.User)
                .Include(x => x.Tags)
                .Include(x => x.PropInts)
                .Include(x => x.PropBools)
                .Include(x => x.PropStrs)
                .Include(x => x.PropTexts)
                .ToListAsync();
            return items;
        }

        public async Task RemoveAsync(Item item)
        {
            _uow.Items.Remove(item);
            await _uow.SaveAsync();
        }

        public async Task<Item> Update(Item entity)
        {
            _uow.Items.Update(entity);
            await _uow.SaveAsync();
            return entity;
        }

        public void Remove(Item item)
        {
            _uow.Items.Remove(item);
            _uow.SaveAsync();
        }

        public async Task<List<Item>> SearchTag(string text)
        {
            var result = new List<Item>();
            var list = await _uow.Items
                .Get().Include(f=>f.Tags)
                .Include(x => x.Likes)
                .Include(x => x.UserCollection)
                .ThenInclude(u => u.User)
                .Include(x => x.PropInts)
                .Include(x => x.PropBools)
                .Include(x => x.PropStrs)
                .Include(x => x.PropTexts)
                .ToListAsync();

            foreach (var item in list)
            {
                foreach (var tag in item.Tags)
                {
                    if (tag.Text == text)
                    {
                        result.Add(item);
                    }
                }
            }
            return result;
        }
    }
}
