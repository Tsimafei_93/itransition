﻿using WebApp.Data.Models;

namespace WebApp.Services.Interfaces
{
    public interface IUserService:IGenericService<User>
    {
        public Task<User> FindById(string id);
        public Task RemoveAsync(User user);

    }
}
