﻿using WebApp.Data.Models;

namespace WebApp.Services.Interfaces
{
    public interface ITagService:IGenericService<ItemTag>
    {
        public Task<List<ItemTag>> Get(int id);
    }
}
