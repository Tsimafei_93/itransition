﻿using WebApp.Data.Models;

namespace WebApp.Services.Interfaces
{
    public interface IUserCollectionService : IGenericService<UserCollection>
    {
        public Task<List<UserCollection>> Get(string id);
        public Task RemoveAsync (UserCollection coll);
        public Task<List<UserCollection>> Search(string text);

    }
}
