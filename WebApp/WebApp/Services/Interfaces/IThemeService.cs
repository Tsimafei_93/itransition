﻿using WebApp.Data.Models;

namespace WebApp.Services.Interfaces
{
    public interface IThemeService:IGenericService<Theme>
    {
        public Task RemoveAsync(Theme theme);
    }
}
