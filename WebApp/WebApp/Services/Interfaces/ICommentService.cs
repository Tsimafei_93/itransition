﻿using WebApp.Data.Models;

namespace WebApp.Services.Interfaces
{
    public interface ICommentService: IGenericService<Comment>
    {
        public Task<List<Comment>> Get(int id);
        public Task<List<Comment>> Search(string text);
    }
}
