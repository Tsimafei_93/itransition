﻿using WebApp.Data.Models;

namespace WebApp.Services.Interfaces
{
    public interface ILikeService:IGenericService<ItemLike>
    {
        public Task<List<ItemLike>> Get(int id);
        public Task RemoveAsync(ItemLike like);
    }
}
