﻿using WebApp.Data.Models;

namespace WebApp.Services.Interfaces
{
    public interface IGenericService<T>
        where T : class
    {
        public abstract List<T> Get();
        public abstract Task<T> FindById(int id);
        public abstract Task<T> Create(T entity);
        public abstract Task<T> Update(T entity);
        public abstract void Remove(T entity);
    }
}
