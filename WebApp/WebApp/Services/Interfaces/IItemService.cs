﻿using WebApp.Data.Models;

namespace WebApp.Services.Interfaces
{
    public interface IItemService:IGenericService<Item>
    {
        public Task<List<Item>> GetColl(int id);
        Task RemoveAsync(Item item);
        public Task<List<Item>> SearchTag(string text);

    }
}
