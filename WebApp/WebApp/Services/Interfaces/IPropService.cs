﻿namespace WebApp.Services.Interfaces
{
    public interface IPropService<T> : IGenericService<T>
        where T : class 
    {
        public Task<List<T>> Get(int id);
        public Task<List<T>> GetByItem(int id);
    }
}
