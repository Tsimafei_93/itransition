﻿using Microsoft.EntityFrameworkCore;
using WebApp.Data.Models;
using WebApp.Data.UOW;
using WebApp.Services.Interfaces;

namespace WebApp.Services
{
    public class PropBoolService : IPropService<PropertyBool>
    {
        private readonly IUnitOfWork _uow;
        public PropBoolService(IUnitOfWork uow)
        {
            _uow = uow;
        }


        public Task<PropertyBool> Create(PropertyBool entity)
        {
            throw new NotImplementedException();
        }

        public Task<PropertyBool> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public List<PropertyBool> Get()
        {
            throw new NotImplementedException();
        }

        public async Task<List<PropertyBool>> Get(int id)
        {
            return await _uow.PropertyBools.Get()
                .Where(u => u.UserCollection.Id == id)
                .Include(u => u.UserCollection)
                .ToListAsync();
        }

        public async Task<List<PropertyBool>> GetByItem(int id)
        {
            return await _uow.PropertyBools.Get()
                .Where(u => u.Item.Id == id)
                .ToListAsync();
        }

        public void Remove(PropertyBool entity)
        {
            throw new NotImplementedException();
        }

        public Task<PropertyBool> Update(PropertyBool entity)
        {
            throw new NotImplementedException();
        }
    }
}
