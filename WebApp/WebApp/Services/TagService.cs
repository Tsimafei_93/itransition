﻿using Microsoft.EntityFrameworkCore;
using WebApp.Data.Models;
using WebApp.Data.UOW;

namespace WebApp.Services.Interfaces
{
    public class TagService : ITagService
    {
        private readonly IUnitOfWork _uow;
        public TagService(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public async Task<ItemTag> Create(ItemTag entity)
        {
            _uow.ItemTags.Create(entity);
            await _uow.SaveAsync();
            return entity;
        }

        public async Task<ItemTag> FindById(int id)
        {
            return await _uow.ItemTags.FindById(id);
        }

        public async Task<List<ItemTag>> Get(int id)
        {
            return await _uow.ItemTags.Get().ToListAsync();
        }

        public List<ItemTag> Get()
        {
            return _uow.ItemTags.Get().ToList();
        }

        public void Remove(ItemTag entity)
        {
            _uow.ItemTags.Remove(entity);
        }

        public async Task<ItemTag> Update(ItemTag entity)
        {
            _uow.ItemTags.Update(entity);
            await _uow.SaveAsync();
            return entity;
        }
    }
}
