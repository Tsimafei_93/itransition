﻿using System.ComponentModel.DataAnnotations;
using WebApp.Data.Models;

namespace WebApp.Models
{
    public class CollectionModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public string UserId { get; set; }
        public int ThemeId { get; set; }
        public Theme Theme { get; set; }
        [Required]
        public ICollection<Theme> Themes { get; set; }
        public ICollection<Item> Items { get; set; }
        public string Image { get; set; }
        [Required]
        public IFormFile ImageData { get; set; }
        public User User { get; set; }
        public List<string>? PropIntName { get; set; }
        public List<string>? PropStrName { get; set; }
        public List<string>? PropBoolName { get; set; }
        public List<string>? PropTextName { get; set; }

    }
}
