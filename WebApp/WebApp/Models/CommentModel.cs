﻿using WebApp.Data.Models;

namespace WebApp.Models
{
    public class CommentModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public  User User { get; set; }
        public int ItemId { get; set; }
        public int CollId { get; set; }
    }
}
