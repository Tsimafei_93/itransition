﻿using WebApp.Data.Models;

namespace WebApp.Models
{
    public class ThemeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
