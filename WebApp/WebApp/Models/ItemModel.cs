﻿using System.ComponentModel.DataAnnotations;
using WebApp.Data.Models;

namespace WebApp.Models
{
    public class ItemModel
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string UserId { get; set; }
        public int UserCollId { get; set; }
        public string? TagText { get; set; }
        public ICollection<ItemTag>? Tags { get; set; }
        public ICollection<ItemLike>? Likes { get; set; }
        public ICollection<Comment>? Comments { get; set; }
        public List<PropertyInt>? PropInt { get; set; }
        public List<PropertyString>? PropStr { get; set; }
        public List<PropertyBool>? PropBools { get; set; }
        public List<PropertyText>? PropText{ get; set; }
    }
}
