﻿namespace WebApp.Models
{
    public class PropModel<T>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public T Value { get; set; }
        public int UserCollId { get; set; }
        public int ItemId { get; set; }
    }
}
