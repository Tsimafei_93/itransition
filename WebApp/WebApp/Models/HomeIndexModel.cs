﻿using WebApp.Data.Models;

namespace WebApp.Models
{
    public class HomeIndexModel
    {
        public List<UserCollection> Collections { get; set; }
        public List<Item> Items { get; set; }
        public List<ItemTag> Tags { get; set; }


        public HomeIndexModel(List<UserCollection> coll, List<Item> items, List<ItemTag> tags)
        {
            Collections = coll;
            Items = items;
            Tags = tags;
        }

        public HomeIndexModel(List<UserCollection> coll, List<Item> items)
        {
            Collections = coll;
            Items = items;
        }
    }
}
