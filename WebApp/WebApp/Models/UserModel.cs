﻿using WebApp.Data.Models;

namespace WebApp.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public List<UserCollection> UserCollections { get; set; }
    }
}
