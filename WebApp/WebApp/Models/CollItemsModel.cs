﻿namespace WebApp.Models
{
    public class CollItemsModel
    {
        public int CollItemId { get; set; }
        public string UserId { get; set; }
        public List<ItemModel> Items { get; set; }
    }
}
