﻿using WebApp.Data.Models;

namespace WebApp.Models
{
    public class LikeModel
    {
        public int Id { get; set; }
        public List<User>? Users { get; set; }
        public int ItemId { get; set; }
    }
}
