﻿using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.Data.Models;

namespace WebApp.Models
{
    public class SortCollModel
    {
        public SelectList Themes { get; set; }
        public List<CollectionModel> Collections { get; set; }
    }
}
