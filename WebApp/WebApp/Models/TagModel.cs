﻿using WebApp.Data.Models;

namespace WebApp.Models
{
    public class TagModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int ItemId { get; set; }
        public List<Item>? Item { get; set; }
    }
}
