using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebApp.Data.Models;
using WebApp.Models;
using WebApp.Services.Interfaces;

namespace WebApp.Areas.Identity.Pages.Account.Manage
{
    public class GetUserCollModel : PageModel
    {
        private readonly IUserCollectionService _userCollService;
        private readonly UserManager<User> _userManager;

        public GetUserCollModel(UserManager<User> userManager,
            IUserCollectionService userCollService)
        {
            _userCollService = userCollService;
            _userManager = userManager;
        }

        [BindProperty]
        public GetUserCollModel Input { get; set; }

        public List<CollectionModel> Collections { get; set; }

        public string Username { get; set; }

        private async Task LoadAsync(User user)
        {
            var userName = await _userManager.GetUserIdAsync(user);
            Username = userName;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await _userManager.GetUserAsync(User);
            await LoadAsync(user);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<UserCollection, CollectionModel>());
            var mapper = new Mapper(config);
            Collections = mapper.Map<List<CollectionModel>>(await _userCollService.Get(Username));
            return Page();
        }
    }
}
