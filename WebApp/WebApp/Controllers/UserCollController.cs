﻿using AutoMapper;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Security.Claims;
using WebApp.Data.Models;
using WebApp.Models;
using WebApp.Services.Interfaces;
using Westwind.AspNetCore.Markdown;

namespace WebCollections.Controllers
{

    public class UserCollController : Controller
    {
        private readonly IUserCollectionService _userCollService;
        private readonly IThemeService _themeService;
        private readonly IUserService _userService;

        public UserCollController(
            IUserCollectionService userCollService,
            IUserService userService,
            IThemeService themeService)
        {
            _userCollService = userCollService;
            _userService = userService;
            _themeService = themeService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<UserCollection, CollectionModel>());
            var mapper = new Mapper(config);
            var coll = mapper.Map<List<CollectionModel>>(_userCollService.Get());
            return View(coll);
        }

        [HttpGet]
        public async Task<IActionResult> GetColl()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<UserCollection, CollectionModel>());
            var mapper = new Mapper(config);
            var coll = mapper.Map<List<CollectionModel>>(await _userCollService.Get(userId));
            return View("Get", coll);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> CreateColl()
        {
            var coll = new CollectionModel();
            coll.Themes = _themeService.Get();
            return View(coll);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateColl(CollectionModel userColl)
        {

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            if (userId == null)
            {
                return View("~/Areas/Identity/Pages/Account/Login.cshtml");
            }

            var file = userColl.ImageData;
            var propInt = new List<PropertyInt>(3);
            var propStr = new List<PropertyString>(3);
            var propBool = new List<PropertyBool>(3);
            var propText = new List<PropertyText>(3);

            foreach (var i in userColl.PropIntName)
            {
                if (i != null)
                    propInt.Add(new PropertyInt()
                    { Name = i });
            }

            foreach (var i in userColl.PropStrName)
            {
                if (i != null)
                    propStr.Add(new PropertyString()
                    { Name = i });
            }

            foreach (var i in userColl.PropBoolName)
            {
                if (i != null)
                    propBool.Add(new PropertyBool()
                    { Name = i });
            }

            foreach (var i in userColl.PropTextName)
            {
                if (i != null)
                    propText.Add(new PropertyText()
                    { Name = i });
            }

            string connectionString = Environment.GetEnvironmentVariable("AZURE_STORAGE_CONNECTION_STRING");

            var blobServiceClient = new BlobServiceClient(connectionString);
            var containerClient = blobServiceClient.GetBlobContainerClient(userId);
            await containerClient.CreateIfNotExistsAsync(PublicAccessType.Blob);

            var files = new List<string>();
            var fileName = file.FileName;
            await foreach (var blob in containerClient.GetBlobsAsync())
            {
                files.Add(blob.Name);
            }
            while (files.Contains(file.FileName))
            {
                fileName = "newFile_" + file.FileName;
            }

            var blobClient = containerClient.GetBlobClient(fileName);

            var httpHeaders = new BlobHttpHeaders()
            {
                ContentType = file.ContentType
            };

            await blobClient.UploadAsync(file.OpenReadStream(), httpHeaders);

            var coll = new UserCollection
            {
                Theme = await _themeService.FindById(userColl.ThemeId),
                Name = userColl.Name,
                Description = Markdown.Parse(userColl.Description),
                Image = blobClient.Uri.ToString(),
                User = await _userService.FindById(userId),
                PropBools = propBool,
                PropInts = propInt,
                PropStrs = propStr,
                PropTexts = propText,
            };
            await _userCollService.Create(coll);
            return RedirectToAction("Create", "Item", coll);
        }

        [HttpGet]
        public async Task<IActionResult> GetById(int id, string UserId)
        {
            var coll = await _userCollService.FindById(id);
            coll.UserId = UserId;
            return RedirectToAction("Get", "Item", coll);
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> DeleteColl(CollectionModel model)
        {
            var coll = await _userCollService.FindById(model.Id);
            if (coll == null)
            {
                return View("Error");
            }
            else
            {
                await _userCollService.RemoveAsync(coll);
                return RedirectToAction("Get");
            }
        }

        public ActionResult GetCollSort(int? theme)
        
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<UserCollection, CollectionModel>());
            var mapper = new Mapper(config);
            var coll = mapper.Map<List<CollectionModel>>(_userCollService.Get());

            if (theme != null && theme != 0)
            {
                coll = coll.Where(p => p.ThemeId == theme).ToList();
            }

            var themes = _themeService.Get();
            // устанавливаем начальный элемент, который позволит выбрать всех
            themes.Insert(0, new Theme { Id = 0, Name = "All",});

            var viewModel = new SortCollModel
            {
                Collections = coll.ToList(),
                Themes = new SelectList(themes, "Id", "Name"),
            };

            return View(viewModel);
        }






    }
}
