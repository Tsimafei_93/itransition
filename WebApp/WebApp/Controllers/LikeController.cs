﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using WebApp.Data.Models;
using WebApp.Models;
using WebApp.Services.Interfaces;

namespace WebCollections.Controllers
{
    [Authorize]
    public class LikeController : Controller
    {
        private readonly ILikeService _likeService;
        private readonly IUserService _userService;
        private readonly IItemService _itemService;
        public LikeController(ILikeService likeService,
            IUserService userService,
            IItemService itemService)
        {
            _likeService = likeService;
            _userService = userService;
            _itemService = itemService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> GetByItem(int id)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ItemLike, LikeModel>());
            var mapper = new Mapper(config);
            var item = mapper.Map<LikeModel>(await _likeService.FindById(id));
            return PartialView(item);
        }

        public async Task<IActionResult> AddLike(int ItemId)
        {
            var actionResult = false;
            var result = false;
            var user = await _userService.FindById(
                User.FindFirstValue(ClaimTypes.NameIdentifier));
            var like = await _likeService.Get(ItemId);

            foreach (var item in like.ToList())
            {
                if (item.User.Id == user.Id)
                {
                    result = true;
                    await _likeService.RemoveAsync(item);
                    actionResult = true;
                }
            }
            if (result == false)
            {
                var newLike = new ItemLike()
                {
                    Item = await _itemService.FindById(ItemId),
                    User = user,
                    CreatedDate = DateTime.Now,
                };
                await _likeService.Create(newLike);
                actionResult = true;
            }
            return Json(actionResult);
        }
    }
}