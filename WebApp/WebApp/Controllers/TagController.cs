﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Data.Models;
using WebApp.Models;
using WebApp.Services.Interfaces;

namespace WebApp.Controllers
{
    //[Authorize]
    public class TagController : Controller
    {
        private readonly ITagService _tagService;
        private readonly IItemService _itemService;

        public TagController(ITagService tagService, IItemService itemService)
        {
            _tagService = tagService;
            _itemService = itemService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetTags()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ItemTag, TagModel>());
            var mapper = new Mapper(config);
            var tags = mapper.Map<List<TagModel>>(_tagService.Get());

            return View(tags);
        }

        [HttpGet]
        public ActionResult AutoComplete(string term)
        {
            var tags = new List<string>();
            if (term != null)
            {
                tags = _tagService.Get()
                    .Where(p => p.Text.Contains(term))
                    .Select(p => p.Text).ToList();
            }

            return Json(tags);
        }

        [HttpGet]
        public async Task<IActionResult> AddTags()
        { return View(); }


        [HttpPost]
        public async Task<IActionResult> AddTags(TagModel tag)
        {
            char[] delimiterChars = { ' ', ',', '.', ':' };
            string text = tag.Text;
            string[] words = text.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);

            var newTags = new List<ItemTag>();
            foreach (var i in words)
            {
                newTags.Add(new ItemTag { Text = i.ToLower() });
            }

            foreach (var i in newTags)
            { await _tagService.Create(i); }
            return View();
        }

    }
}
