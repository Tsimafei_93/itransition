﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Data.Models;
using WebApp.Models;
using WebApp.Services.Interfaces;

namespace WebCollections.Controllers
{
    public class ItemController : Controller
    {
        private readonly IItemService _itemService;
        private readonly IUserCollectionService _userCollService;
        private readonly IPropService<PropertyInt> _intService;
        private readonly IPropService<PropertyBool> _boolService;
        private readonly IPropService<PropertyString> _strService;
        private readonly IPropService<PropertyText> _textService;
        private readonly ICommentService _commentService;

        public ItemController(IItemService itemService,
            IUserCollectionService userCollService,
            IPropService<PropertyInt> intService,
            IPropService<PropertyBool> boolService,
            IPropService<PropertyString> strService,
            IPropService<PropertyText> textService,
            ICommentService commentService)
        {
            _itemService = itemService;
            _userCollService = userCollService;
            _intService = intService;
            _boolService = boolService;
            _strService = strService;
            _textService = textService;
            _commentService = commentService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Get(UserCollection coll)
        {
            var it = await _itemService.GetColl(coll.Id);
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Item, ItemModel>());
            var mapper = new Mapper(config);
            var items = mapper.Map<List<ItemModel>>(it);
            int i = 0;
            foreach (var item in items.ToList())
            {
                item.UserCollId = coll.Id;
                item.PropInt = it[i].PropInts?.ToList();
                item.PropBools = it[i].PropBools?.ToList();
                item.PropStr = it[i].PropStrs?.ToList();
                item.PropText = it[i].PropTexts?.ToList();
                item.Tags = it[i].Tags?.ToList();
                item.UserId = it.First().UserCollection.User.Id;
                i++;
            }
            var itemsModel = new CollItemsModel() { CollItemId = coll.Id, Items = items, UserId = coll.UserId };
            return View(itemsModel);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Create(int id)
        {
            var propInt = await _intService.Get(id);
            var propStr = await _strService.Get(id);
            var propBool = await _boolService.Get(id);
            var propText = await _textService.Get(id);

            var model = new ItemModel()
            {
                UserCollId = id,
                PropInt = propInt,
                PropStr = propStr,
                PropBools = propBool,
                PropText = propText,
            };
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create(ItemModel item)  

        {
            char[] delimiterChars = { ' ', ',', '.', ':' };
            string text = item.TagText;
            var newTags = new List<ItemTag>();
            if (text != null) 
            {
                string[] words = text.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);

                foreach (var i in words)
                {
                    newTags.Add(new ItemTag { Text = i.ToLower() });
                }
            }
            var coll = await _userCollService
                .FindById(item.UserCollId);

            var newItem = new Item
            {
                Name = item.Name,
                CreateData = DateTime.Now,
                UserCollection = coll,
                PropInts = item.PropInt,
                PropStrs = item.PropStr,
                PropBools = item.PropBools,
                PropTexts = item.PropText,
                Tags = newTags,
            };
            await _itemService.Create(newItem);
            return RedirectToAction("Get", coll);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> DeleteItem(ItemModel model)
        {

            var item = await _itemService.FindById(model.Id);
            if (item == null)
            {
                return View("Error");
            }
            else
            {
                var id = await _userCollService.FindById(model.UserCollId);
                await _itemService.RemoveAsync(item);
                return RedirectToAction("Get", "Item", id);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetById(int id)
        {
            var it = await _itemService.FindById(id);

            var config = new MapperConfiguration(cfg => cfg.CreateMap<Item, ItemModel>());
            var mapper = new Mapper(config);
            var item = mapper.Map<ItemModel>(it);

            item.PropInt = it.PropInts?.ToList();
            item.PropBools = it.PropBools?.ToList();
            item.PropStr = it.PropStrs?.ToList();
            item.PropText = it.PropTexts?.ToList();
            item.Likes = it.Likes?.ToList();
            item.Tags = it.Tags?.ToList();

            return View(item);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> EditItem(int Id, int UserCollId)
        {
            var it = await _itemService.FindById(Id);

            var config = new MapperConfiguration(cfg => cfg.CreateMap<Item, ItemModel>());
            var mapper = new Mapper(config);
            var item = mapper.Map<ItemModel>(it);

            item.PropInt = it.PropInts?.ToList();
            item.PropBools = it.PropBools?.ToList();
            item.PropStr = it.PropStrs?.ToList();
            item.PropText = it.PropTexts?.ToList();
            item.Tags = it.Tags?.ToList();
            item.UserCollId = UserCollId;

            return View(item);
        }  

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> EditItem(ItemModel model)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ItemModel, Item>());
            var mapper = new Mapper(config);
            var newitem = mapper.Map<Item>(model);

            newitem.PropBools = model.PropBools?.ToList();
            newitem.PropInts = model.PropInt?.ToList();
            newitem.PropStrs = model.PropStr?.ToList();
            newitem.PropTexts = model.PropText?.ToList();
            newitem.Tags = model.Tags?.ToList();

            await _itemService.Update(newitem);
            return RedirectToAction("GetById", model);
        }
    }
}



