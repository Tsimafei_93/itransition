﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using WebApp.Data.Models;
using WebApp.Models;
using WebApp.Services.Interfaces;

namespace WebCollections.Controllers
{
    [Authorize]
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;
        private readonly IUserService _userService;
        private readonly IItemService _itemService;


        public CommentController(ICommentService commentService,
            IUserService userService,
            IItemService itemService)
        {
            _commentService = commentService;
            _userService = userService;
            _itemService = itemService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Get(int id)
        {
            MapperConfiguration config = new MapperConfiguration(
                cfg => cfg.CreateMap<Comment, CommentModel>());
            Mapper mapper = new Mapper(config);
            var comments = mapper.Map<List<CommentModel>>(
               await _commentService.Get(id));
            return PartialView("_GetByItem.cshtml", comments);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public async Task<IActionResult> Create(CommentModel comment)
        {
            var user = await _userService.FindById(
                User.FindFirstValue(ClaimTypes.NameIdentifier));
            var newCom = new Comment
            {
                CreatedDate = DateTime.Now,
                Text = comment.Text,
                Items = await _itemService
                .FindById(comment.ItemId),
                User = user,
            };
            await _commentService.Create(newCom);
            return RedirectToAction("GetById","Item", new { id = comment.ItemId });
        }
    }
}
