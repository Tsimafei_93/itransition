﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Data.Models;
using WebApp.Models;
using WebApp.Services.Interfaces;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ThemeController : Controller
    {
        private readonly IThemeService _themeService;

        public ThemeController(IThemeService themeService)
        {
            _themeService = themeService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> GetThemes()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Theme, ThemeModel>());
            var mapper = new Mapper(config);
            
            var items = mapper.Map<List<ThemeModel>>(_themeService.Get());

            return View(items);
        }

        [HttpGet]
        public async Task<IActionResult> CreateTheme()
        {
            return PartialView("_CreateTheme");
        }



        [HttpPost]
        public async Task<IActionResult> CreateTheme(ThemeModel theme)
        {

            var item = new Theme() {Name = theme.Name };
            
            await _themeService.Create(item);

            return RedirectToAction("GetThemes");
        }


    }
}
