﻿using AutoMapper;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data.Models;
using WebApp.Models;
using WebApp.Services.Interfaces;

namespace WebCollections.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserCollectionService _userCollectionService;
        private readonly IItemService _itemService;
        private readonly ITagService _tagService;
        private readonly ICommentService _commentService;


        public HomeController(IUserCollectionService userCollectionService,
            IItemService itemService,
            ITagService tagService,
            ICommentService commentService)
        {
            _userCollectionService = userCollectionService;
            _itemService = itemService;
            _tagService = tagService;
            _commentService = commentService;
        }

        public IActionResult Index()
        {
            var cc = new CollComparer();

            var dataItems = _itemService.Get();
            dataItems.Reverse();
            var modelItems = dataItems.Take(6).ToList();

            var dataColl = _userCollectionService.Get();
            dataColl.Sort(cc);
            dataColl.Reverse();
            var modelColl = dataColl.Take(6).ToList();

            var dataTag = _tagService.Get().ToList();
            dataTag.Distinct();

            var model = new HomeIndexModel(modelColl,modelItems, dataTag);

            return View(model);
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );
            return LocalRedirect(returnUrl);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SetTheme(string data)
        {
            if (data == "White")
            {
                data = "Dark";
            }
            else { data = "White"; }

            var cookies = new CookieOptions();
            cookies.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Append("theme", data, cookies);
            return Ok();
        }

        public async Task<IActionResult> Search(string search)
        {
            if (search == null || search == " ")
            {
                return RedirectToAction("Index");
            }
            var results = await _userCollectionService.Search(search);

            var itemsid = new List<int>();
            var items = new List<Item>();

            var comm = await _commentService.Search(search);

            foreach (var i in comm)
            {
                itemsid.Add(i.Items.Id);
            }

            itemsid.Distinct();

            foreach (var i in itemsid)
            {
                var item = await _itemService.FindById(i);
                items.Add(item) ;
            }

            var model = new HomeIndexModel(results, items);

            return View(model);
        }

        public async Task<IActionResult> SearchTag(string search)
        {
            if (search == null || search == " ")
            {
                return RedirectToAction("Index");
            }
            var results = await _itemService.SearchTag(search);

            var config = new MapperConfiguration(cfg => cfg.CreateMap<Item, ItemModel>());
            var mapper = new Mapper(config);
            var coll = mapper.Map<List<ItemModel>>(results);
            return View(coll);
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public class CollComparer : IComparer<UserCollection>
        {
            public int Compare(UserCollection x, UserCollection y)
            {
                if (x == null)
                {
                    if (y == null)
                    {
                        // If x is null and y is null, they're
                        // equal.
                        return 0;
                    }
                    else
                    {
                        // If x is null and y is not null, y
                        // is greater.
                        return -1;
                    }
                }
                else
                {
                    // If x is not null...
                    //
                    if (y == null)
                    // ...and y is null, x is greater.
                    {
                        return 1;
                    }
                    else
                    {
                        // ...and y is not null, compare the
                        // lengths of the two strings.
                        //
                        int retval = x.Items.Count().CompareTo(y.Items.Count());

                        if (retval != 0)
                        {
                            // If the strings are not of equal length,
                            // the longer string is greater.
                            //
                            return retval;
                        }
                        else
                        {
                            // If the strings are of equal length,
                            // sort them with ordinary string comparison.
                            //
                            return x.Items.Count().CompareTo(y.Items.Count());
                        }
                    }
                }
            }
        }
    }
}
