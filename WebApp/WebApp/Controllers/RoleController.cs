﻿using AutoMapper;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApp.Data.Models;
using WebApp.Models;
using WebApp.Services.Interfaces;
using Westwind.AspNetCore.Markdown;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<Data.Models.User> _userManager;
        private readonly IThemeService _themeService;
        private readonly IUserCollectionService _userCollService;
        private readonly IUserService _userService;


        public RoleController(RoleManager<IdentityRole> roleManager,
            UserManager<Data.Models.User> userManager,
            IThemeService themeService,
            IUserCollectionService userCollService,
            IUserService userService)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _themeService = themeService;
            _userCollService = userCollService;
            _userService = userService;
        }

        public IActionResult Index() => View(_roleManager.Roles.ToList());
        public IActionResult Create() => View();

        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                IdentityResult result = await _roleManager.CreateAsync(new IdentityRole(name));
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(name);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            IdentityRole role = await _roleManager.FindByIdAsync(id);
            if (role != null)
            {
                IdentityResult result = await _roleManager.DeleteAsync(role);
            }
            return RedirectToAction("Index");
        }
        [Authorize(Roles = "Admin")]
        public IActionResult UserList()
        {
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.CreateMap<User, UserModel>());
            Mapper mapper = new Mapper(config);
            var users = mapper.Map<List<UserModel>>(_userManager.Users.ToList());
            return View(users);
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(string userId)
        {
            // получаем пользователя
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                // получем список ролей пользователя
                var userRoles = await _userManager.GetRolesAsync(user);
                var allRoles = _roleManager.Roles.ToList();
                var model = new ChangeRoleModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserRoles = userRoles,
                    AllRoles = allRoles
                };
                return View(model);
            }

            return NotFound();
        }


        [HttpPost]
        public async Task<IActionResult> Edit(string userId, List<string> roles)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var userRoles = await _userManager.GetRolesAsync(user);

                var allRoles = _roleManager.Roles.ToList();

                var addedRoles = roles.Except(userRoles);

                var removedRoles = userRoles.Except(roles);

                await _userManager.AddToRolesAsync(user, addedRoles);

                await _userManager.RemoveFromRolesAsync(user, removedRoles);

                return RedirectToAction("UserList");
            }

            return NotFound();
        }


        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> CreateColl(string id)
        {
            var coll = new CollectionModel();
            coll.Themes = _themeService.Get();
            coll.UserId = id;
            return View(coll);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> CreateColl(CollectionModel userColl)
        {
            if (userColl.UserId == null)
            {
                return View("~/Areas/Identity/Pages/Account/Login.cshtml");
            }

            var file = userColl.ImageData;
            var propInt = new List<PropertyInt>(3);
            var propStr = new List<PropertyString>(3);
            var propBool = new List<PropertyBool>(3);
            var propText = new List<PropertyText>(3);

            foreach (var i in userColl.PropIntName)
            {
                if (i != null)
                    propInt.Add(new PropertyInt()
                    { Name = i });
            }

            foreach (var i in userColl.PropStrName)
            {
                if (i != null)
                    propStr.Add(new PropertyString()
                    { Name = i });
            }

            foreach (var i in userColl.PropBoolName)
            {
                if (i != null)
                    propBool.Add(new PropertyBool()
                    { Name = i });
            }

            foreach (var i in userColl.PropTextName)
            {
                if (i != null)
                    propText.Add(new PropertyText()
                    { Name = i });
            }

            string connectionString = Environment.GetEnvironmentVariable("AZURE_STORAGE_CONNECTION_STRING");

            var blobServiceClient = new BlobServiceClient(connectionString);
            var containerClient = blobServiceClient.GetBlobContainerClient(userColl.UserId);
            await containerClient.CreateIfNotExistsAsync(PublicAccessType.Blob);

            var files = new List<string>();
            var fileName = file.FileName;
            await foreach (var blob in containerClient.GetBlobsAsync())
            {
                files.Add(blob.Name);
            }
            while (files.Contains(file.FileName))
            {
                fileName = "newFile_" + file.FileName;
            }

            var blobClient = containerClient.GetBlobClient(fileName);

            var httpHeaders = new BlobHttpHeaders()
            {
                ContentType = file.ContentType
            };

            await blobClient.UploadAsync(file.OpenReadStream(), httpHeaders);

            var coll = new UserCollection
            {
                Theme = await _themeService.FindById(userColl.ThemeId),
                Name = userColl.Name,
                Description = Markdown.Parse(userColl.Description),
                Image = blobClient.Uri.ToString(),
                User = await _userManager.FindByIdAsync(userColl.UserId),
                PropBools = propBool,
                PropInts = propInt,
                PropStrs = propStr,
                PropTexts = propText,
            };
            await _userCollService.Create(coll);
            return RedirectToAction("Create", "Item", coll);
        }
        
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Block(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                user.LockoutEnd = DateTime.MaxValue;
                await _userService.Update(user);
                await _userManager.UpdateSecurityStampAsync(user);
                await _userService.Update(user);
                return RedirectToAction("Get", "User");
            }
            return NotFound();
        }
        
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Unblock(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                user.LockoutEnd = null;
                await _userService.Update(user);
                return RedirectToAction("Get", "User");
            }
            return NotFound();
        }
    }
}
