﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Data.Models;
using WebApp.Models;
using WebApp.Services.Interfaces;

namespace WebCollections.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Get()
        {
            MapperConfiguration config = new MapperConfiguration(cfg => cfg.CreateMap<User, UserModel>());
            Mapper mapper = new Mapper(config);
            var users = mapper.Map<List<UserModel>>(_userService.Get());
            return View(users);
        }

        [HttpPost]
        public async Task<IActionResult> EditUser(UserModel model)
        {
            var user = await _userService.FindById(model.Id);
            if (user == null)
            {
                return View("Error");
            }
            else
            {
                user.UserName = model.UserName;
                user.Email = model.Email;
                await _userService.Update(user);
                return RedirectToAction("Get");
            }
        }

        [HttpGet]
        public async Task<IActionResult> EditUser(string id)
        {
            var user = await _userService.FindById(id);
            if (user == null)
            {
                return View("Error");
            }
            var config = new MapperConfiguration(cfg => cfg.CreateMap<User, UserModel>());
            var mapper = new Mapper(config);
            var userModel = mapper.Map<UserModel>(await _userService.Update(user));
            return View(userModel);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteUser(UserModel model)
        {
            var user = await _userService.FindById(model.Id);
            if (user == null)
            {
                return View("Error");
            }
            else
            {
                await _userService.RemoveAsync(user);
                return RedirectToAction("Get");
            }
        }
    }
}
